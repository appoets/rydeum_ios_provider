/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Deliveries : Mappable {
	var id : Int?
	var delivery_request_id : Int?
	var user_id : String?
	var provider_id : Int?
	var geofence_id : String?
	var package_type_id : Int?
	var status : String?
	var admin_service : String?
	var paid : Int?
	var provider_rated : Int?
	var distance : Int?
	var weight : Int?
	var length : Int?
	var breadth : Int?
	var height : Int?
	var location_points : String?
	var timezone : String?
	var travel_time : String?
	var name : String?
	var mobile : String?
	var payment_mode : String?
	var instruction : String?
	var s_address : String?
	var s_latitude : Int?
	var s_longitude : Int?
	var d_address : String?
	var d_latitude : Double?
	var d_longitude : Double?
	var track_distance : Int?
	var destination_log : String?
	var unit : String?
	var is_fragile : Int?
	var currency : String?
	var track_latitude : Int?
	var track_longitude : Int?
	var otp : String?
	var assigned_at : String?
	var schedule_at : String?
	var started_at : String?
	var finished_at : String?
	var surge : Int?
	var route_key : String?
	var admin_id : String?
	var payment : Payment?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		delivery_request_id <- map["delivery_request_id"]
		user_id <- map["user_id"]
		provider_id <- map["provider_id"]
		geofence_id <- map["geofence_id"]
		package_type_id <- map["package_type_id"]
		status <- map["status"]
		admin_service <- map["admin_service"]
		paid <- map["paid"]
		provider_rated <- map["provider_rated"]
		distance <- map["distance"]
		weight <- map["weight"]
		length <- map["length"]
		breadth <- map["breadth"]
		height <- map["height"]
		location_points <- map["location_points"]
		timezone <- map["timezone"]
		travel_time <- map["travel_time"]
		name <- map["name"]
		mobile <- map["mobile"]
		payment_mode <- map["payment_mode"]
		instruction <- map["instruction"]
		s_address <- map["s_address"]
		s_latitude <- map["s_latitude"]
		s_longitude <- map["s_longitude"]
		d_address <- map["d_address"]
		d_latitude <- map["d_latitude"]
		d_longitude <- map["d_longitude"]
		track_distance <- map["track_distance"]
		destination_log <- map["destination_log"]
		unit <- map["unit"]
		is_fragile <- map["is_fragile"]
		currency <- map["currency"]
		track_latitude <- map["track_latitude"]
		track_longitude <- map["track_longitude"]
		otp <- map["otp"]
		assigned_at <- map["assigned_at"]
		schedule_at <- map["schedule_at"]
		started_at <- map["started_at"]
		finished_at <- map["finished_at"]
		surge <- map["surge"]
		route_key <- map["route_key"]
		admin_id <- map["admin_id"]
		payment <- map["payment"]
	}

}
